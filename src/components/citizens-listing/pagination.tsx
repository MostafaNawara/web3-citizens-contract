import { ChevronLeftIcon, ChevronRightIcon } from '@heroicons/react/outline';

export const Pagination = ({
    currentPage,
    totalPages,
    setNextPage,
    setPreviousPage,
    isNextEnabled,
    isPreviousEnabled,
}: {
    currentPage: number;
    total: number;
    totalPages: number;
    setNextPage: () => void;
    setPreviousPage: () => void;
    isNextEnabled: boolean;
    isPreviousEnabled: boolean;
}) => {
    return (
        <div className="px-4 py-3 flex items-center justify-between border-t border-gray-100">
            {/* Desktop */}
            <div className="flex-1 flex items-center justify-between">
                <div>
                    <p className="text-sm text-gray-700">
                        Showing page <span className="font-medium">{currentPage}</span> of{' '}
                        <span className="font-medium">{totalPages}</span>
                    </p>
                </div>
                <div>
                    <nav
                        className="relative z-0 inline-flex rounded-md shadow-sm -space-x-px"
                        aria-label="Pagination"
                    >
                        <button
                            type="button"
                            className="relative inline-flex items-center justify-between px-2 py-2 rounded-l-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50 w-20 disabled:cursor-not-allowed disabled:opacity-50"
                            disabled={!isPreviousEnabled}
                            onClick={() => setPreviousPage()}
                        >
                            <ChevronLeftIcon className="h-5 w-5" aria-hidden="true" />
                            <span>Prev</span>
                        </button>
                        <button
                            type="button"
                            className="relative inline-flex items-center justify-between px-2 py-2 rounded-r-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50 w-20 disabled:cursor-not-allowed disabled:opacity-50"
                            disabled={!isNextEnabled}
                            onClick={evt => {
                                evt.preventDefault();
                                setNextPage();
                            }}
                        >
                            <span>Next</span>
                            <ChevronRightIcon className="h-5 w-5" aria-hidden="true" />
                        </button>
                    </nav>
                </div>
            </div>
        </div>
    );
};
