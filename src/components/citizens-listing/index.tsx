import { useCitizensPagination } from '@hooks/use-citizens';
import { Pagination } from '@components/citizens-listing/pagination';
import { PageHeader } from './page-header';
import { Listing } from './listing';

export const CitizensListing = () => {
    const {
        citizens,
        currentPage,
        total,
        totalPages,
        setNextPage,
        setPreviousPage,
        isNextEnabled,
        isPreviousEnabled,
    } = useCitizensPagination();
    return (
        <div className="w-full bg-white shadow-lg rounded-lg border border-gray-200">
            <PageHeader total={total} />

            <div className="p-3">
                <div className="overflow-x-auto">
                    <Listing citizens={citizens} />
                </div>
            </div>
            <footer>
                <Pagination
                    {...{
                        currentPage,
                        total,
                        totalPages,
                        setNextPage,
                        setPreviousPage,
                        isNextEnabled,
                        isPreviousEnabled,
                    }}
                />
            </footer>
        </div>
    );
};
