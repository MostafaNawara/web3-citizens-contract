import { useState } from 'react';
import { EyeIcon } from '@heroicons/react/outline';
import { Citizen } from '@hooks/use-citizens';

import { ViewNote } from './view-note';

export const Listing = ({ citizens }: { citizens: Citizen[] }) => {
    const [selectedCitizen, setSelectedCitizen] = useState<Citizen['id'] | null>(null);
    const [previewNote, setPreviewNote] = useState(false);

    return (
        <>
            <table className="table-auto w-full">
                <thead className="text-xs font-semibold uppercase text-gray-400 bg-gray-50">
                    <tr>
                        {['Name', 'Age', 'City', 'Note'].map((column, idx) => (
                            <th key={idx} className="p-2 whitespace-nowrap text-left">
                                <div className="font-semibold">{column}</div>
                            </th>
                        ))}
                    </tr>
                </thead>
                <tbody className="text-sm divide-y divide-gray-100">
                    {citizens.map((citizen, idx) => (
                        <tr key={idx}>
                            <td className="p-2 whitespace-nowrap">
                                <div className="flex items-center">
                                    <div className="w-10 h-10 flex-shrink-0 mr-2 sm:mr-3">
                                        <div className="rounded-full bg-slate-600 text-white w-8 h-8 leading-8 items-center content-center  text-center">
                                            {citizen.name.charAt(0).toUpperCase()}
                                        </div>
                                    </div>
                                    <div className="font-medium text-gray-800">{citizen.name}</div>
                                </div>
                            </td>
                            <td className="p-2 whitespace-nowrap">
                                <div className="text-left font-medium text-gray-500">
                                    {citizen.age} {citizen.age > 1 ? 'years' : 'year'}
                                </div>
                            </td>
                            <td className="p-2 whitespace-nowrap">
                                <div className="text-left font-medium text-gray-500">
                                    {citizen.city}
                                </div>
                            </td>
                            <td className="p-2 whitespace-nowrap">
                                <button
                                    type="button"
                                    onClick={() => {
                                        setSelectedCitizen(citizen.id);
                                        setPreviewNote(true);
                                    }}
                                >
                                    <EyeIcon className="h-5 w-5" />
                                </button>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
            <ViewNote
                open={previewNote}
                setOpen={setPreviewNote}
                selectedCitizenId={selectedCitizen}
            />
        </>
    );
};
