import { Dispatch, SetStateAction, useRef } from 'react';
import { Dialog } from '@headlessui/react';
import { NewspaperIcon } from '@heroicons/react/outline';
import { useCitizenNote } from '@hooks/use-citizens';

export const ViewNote = ({
    open,
    setOpen,
    selectedCitizenId,
}: {
    open: boolean;
    setOpen: Dispatch<SetStateAction<boolean>>;
    selectedCitizenId: string | null;
}) => {
    const cancelButtonRef = useRef(null);
    const { note, isLoading } = useCitizenNote(selectedCitizenId as string);

    return (
        <Dialog
            open={open}
            onClose={() => setOpen(false)}
            className="fixed z-10 inset-0 overflow-y-auto"
        >
            <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
                <Dialog.Overlay className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
                <span
                    className="hidden sm:inline-block sm:align-middle sm:h-screen"
                    aria-hidden="true"
                >
                    &#8203;
                </span>

                <div className="relative inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full">
                    <div className="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                        <div className="sm:flex sm:items-start">
                            <div className="mx-auto flex-shrink-0 flex items-center justify-center h-12 w-12 rounded-full bg-green-100 sm:mx-0 sm:h-10 sm:w-10">
                                <NewspaperIcon
                                    className="h-6 w-6 text-green-600"
                                    aria-hidden="true"
                                />
                            </div>
                            <div className="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left">
                                <Dialog.Title
                                    as="h3"
                                    className="text-lg leading-6 font-medium text-gray-900"
                                >
                                    Citizen note
                                </Dialog.Title>
                                <div className="mt-2">
                                    {isLoading ? (
                                        <p className="text-sm text-gray-500">Loading ...</p>
                                    ) : (
                                        <p className="text-sm text-gray-500">{note}</p>
                                    )}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                        <button
                            type="button"
                            className="mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm"
                            onClick={() => setOpen(false)}
                            ref={cancelButtonRef}
                        >
                            Cancel
                        </button>
                    </div>
                </div>
            </div>
        </Dialog>
    );
};
