import Link from 'next/link';

export const PageHeader = ({ total }: { total: number }) => {
    return (
        <>
            <header className="flex justify-between items-center px-5 py-4 border-b border-gray-100">
                <h2 className="font-semibold text-gray-800 text-lg">Citizens ({total})</h2>
                <Link href="/citizens/add-new">
                    <a className="focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-700 transition duration-150 ease-in-out hover:bg-green-600 bg-green-700 rounded text-white px-8 py-2 text-sm">
                        Add new citizen
                    </a>
                </Link>
            </header>
        </>
    );
};
