import { Form } from './form';

export const AddNewCitizen = () => {
    return (
        <>
            <div className="container mx-auto w-11/12 md:w-2/3 max-w-lg">
                <div className="relative bg-white shadow-lg rounded-lg border border-gray-200">
                    <header className="px-5 py-4 border-b border-gray-100">
                        <h2 className="font-semibold text-gray-800 text-lg">Add New Citizen</h2>
                    </header>
                    <div className="px-5 py-4">
                        <Form />
                    </div>
                </div>
            </div>
        </>
    );
};
