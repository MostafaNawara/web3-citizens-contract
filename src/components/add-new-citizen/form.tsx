import { useRouter } from 'next/router';
import { Formik, Form as FormikForm, Field, ErrorMessage, FormikHelpers } from 'formik';
import * as Yup from 'yup';
import { useAddCitizenMutation } from '@hooks/use-citizens';
import { Alert } from './alert';

const validationSchema = Yup.object().shape({
    name: Yup.string().min(2, 'Too Short!').max(70, 'Too Long!').required('Required'),
    age: Yup.number().moreThan(0, 'greater than zero!').required('Required'),
    city: Yup.string().min(2, 'Too Short!').max(70, 'Too Long!').required('Required'),
    someNote: Yup.string().min(2, 'Too Short!').max(70, 'Too Long!').required('Required'),
});

type FormValues = {
    name: string;
    age: number;
    city: string;
    someNote: string;
};

export const Form = () => {
    const router = useRouter();
    const { addCitizen, isLoading, error } = useAddCitizenMutation();

    return (
        <>
            {isLoading && <p>Loading...</p>}

            {error && <Alert>{error}</Alert>}
            <Formik
                initialValues={{ name: '', age: 0, city: '', someNote: '' }}
                validationSchema={validationSchema}
                onSubmit={(
                    values: FormValues,
                    { setSubmitting, resetForm }: FormikHelpers<FormValues>,
                ) => {
                    addCitizen({
                        ...values,
                    })
                        .then(() => {
                            setSubmitting(false);
                            resetForm();
                        })
                        .finally(() => {
                            router.push('/citizens');
                        });
                }}
            >
                {({ isSubmitting }) => (
                    <FormikForm>
                        <div className="mb-5">
                            <label
                                htmlFor="name"
                                className="text-gray-800 text-sm font-semibold leading-tight tracking-normal"
                            >
                                Name
                            </label>
                            <Field
                                type="text"
                                name="name"
                                className="mt-2 text-gray-600 focus:outline-none focus:border focus:border-indigo-700 font-normal w-full h-10 flex items-center pl-3 text-sm border-gray-300 rounded border"
                                placeholder="ex. James Don"
                            />
                            <ErrorMessage
                                name="name"
                                component="div"
                                className="text-red-500 text-sm mt-1"
                            />
                        </div>

                        <div className="mb-5">
                            <label
                                htmlFor="age"
                                className="text-gray-800 text-sm font-semibold leading-tight tracking-normal"
                            >
                                Age
                            </label>
                            <Field
                                name="age"
                                type="number"
                                className="mt-2 text-gray-600 focus:outline-none focus:border focus:border-indigo-700 font-normal w-full h-10 flex items-center pl-3 text-sm border-gray-300 rounded border"
                                placeholder="ex. 25"
                            />
                            <ErrorMessage
                                name="age"
                                component="div"
                                className="text-red-500 text-sm mt-1"
                            />
                        </div>

                        <div className="mb-5">
                            <label
                                htmlFor="city"
                                className="text-gray-800 text-sm font-semibold leading-tight tracking-normal"
                            >
                                City
                            </label>
                            <Field
                                name="city"
                                type="text"
                                className="mt-2 text-gray-600 focus:outline-none focus:border focus:border-indigo-700 font-normal w-full h-10 flex items-center pl-3 text-sm border-gray-300 rounded border"
                                placeholder="ex. New York"
                            />
                            <ErrorMessage
                                name="city"
                                component="div"
                                className="text-red-500 text-sm mt-1"
                            />
                        </div>

                        <div className="mb-5">
                            <label
                                htmlFor="someNote"
                                className="text-gray-800 text-sm font-semibold leading-tight tracking-normal"
                            >
                                Note
                            </label>
                            <Field
                                name="someNote"
                                type="text"
                                className="mt-2 text-gray-600 focus:outline-none focus:border focus:border-indigo-700 font-normal w-full h-10 flex items-center pl-3 text-sm border-gray-300 rounded border"
                                placeholder="ex. He is a developer"
                            />
                            <ErrorMessage
                                name="someNote"
                                component="div"
                                className="text-red-500 text-sm mt-1"
                            />
                        </div>

                        <div className="flex items-center justify-start w-full">
                            <button
                                type="submit"
                                disabled={isSubmitting}
                                className="focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-700 transition duration-150 ease-in-out hover:bg-indigo-600 bg-indigo-700 rounded text-white px-8 py-2 text-sm"
                            >
                                Submit
                            </button>
                            <button
                                type="button"
                                onClick={() => router.push('/citizens')}
                                className="focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-400 ml-3 bg-gray-100 transition duration-150 text-gray-600 ease-in-out hover:border-gray-400 hover:bg-gray-300 border rounded px-8 py-2 text-sm"
                            >
                                Cancel
                            </button>
                        </div>
                    </FormikForm>
                )}
            </Formik>
        </>
    );
};
