import { FC, ReactNode } from 'react';
import { Header } from '@components/common/header';

export const Layout: FC<{ children: ReactNode }> = ({ children }) => {
    return (
        <>
            <Header />
            <main className="mt-6">
                <div className="max-w-6xl mx-auto px-2 sm:px-6 lg:px-8">{children}</div>
            </main>
        </>
    );
};
