import Link from 'next/link';
import { useRouter } from 'next/router';
import { Disclosure } from '@headlessui/react';
import { MenuIcon, XIcon } from '@heroicons/react/outline';
import { useMetamask } from '@hooks/use-metamask';

function classNames(...classes: string[]) {
    return classes.filter(Boolean).join(' ');
}

const NavigationDesktop = () => {
    const router = useRouter();
    return (
        <>
            <Link href="/">
                <a
                    className={classNames(
                        router.pathname == '/'
                            ? 'bg-gray-900 text-white'
                            : 'text-gray-300 hover:bg-gray-700 hover:text-white',
                        'px-3 py-2 rounded-md text-sm font-medium',
                    )}
                >
                    Home
                </a>
            </Link>
            <Link href="/citizens">
                <a
                    className={classNames(
                        router.pathname == '/citizens'
                            ? 'bg-gray-900 text-white'
                            : 'text-gray-300 hover:bg-gray-700 hover:text-white',
                        'px-3 py-2 rounded-md text-sm font-medium',
                    )}
                >
                    Citizens
                </a>
            </Link>
        </>
    );
};

const NavigationMobile = () => {
    const router = useRouter();
    return (
        <>
            <Link href="/" passHref>
                <Disclosure.Button
                    as="a"
                    className={classNames(
                        router.pathname == '/'
                            ? 'bg-gray-900 text-white'
                            : 'text-gray-300 hover:bg-gray-700 hover:text-white',
                        'block px-3 py-2 rounded-md text-base font-medium',
                    )}
                >
                    Home
                </Disclosure.Button>
            </Link>
            <Link href="/citizens" passHref>
                <Disclosure.Button
                    as="a"
                    className={classNames(
                        router.pathname == '/citizens'
                            ? 'bg-gray-900 text-white'
                            : 'text-gray-300 hover:bg-gray-700 hover:text-white',
                        'block px-3 py-2 rounded-md text-base font-medium',
                    )}
                >
                    Citizens
                </Disclosure.Button>
            </Link>
        </>
    );
};

const ConnectionStatus = () => {
    const { isMetamaskInstalled, isMetamaskConnected } = useMetamask();

    return (
        <span className="bg-gray-800 p-1 rounded-full text-gray-400">
            <span className="sr-only">View connection Status</span>
            {isMetamaskInstalled && isMetamaskConnected ? (
                <span className="flex h-3 w-3 relative">
                    <span className="animate-ping absolute inline-flex h-full w-full rounded-full bg-green-400 opacity-75"></span>
                    <span className="relative inline-flex rounded-full h-3 w-3 bg-green-500"></span>
                </span>
            ) : null}
        </span>
    );
};

export const Header = () => {
    return (
        <Disclosure as="nav" className="bg-gray-800">
            {({ open }: { open: boolean }) => (
                <>
                    <div className="max-w-6xl mx-auto px-2 sm:px-6 lg:px-8">
                        <div className="relative flex items-center justify-between h-16">
                            <div className="absolute inset-y-0 left-0 flex items-center sm:hidden">
                                {/* Mobile menu button*/}
                                <Disclosure.Button className="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-white hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white">
                                    <span className="sr-only">Open main menu</span>
                                    {open ? (
                                        <XIcon className="block h-6 w-6" aria-hidden="true" />
                                    ) : (
                                        <MenuIcon className="block h-6 w-6" aria-hidden="true" />
                                    )}
                                </Disclosure.Button>
                            </div>
                            <div className="flex-1 flex items-center justify-center sm:items-stretch sm:justify-start">
                                <div className="flex-shrink-0 flex items-center">
                                    <h1 className="text-xl font-extrabold text-transparent bg-clip-text bg-gradient-to-r from-emerald-500 to-lime-600">
                                        CitizensSmartContract
                                    </h1>
                                </div>
                                <div className="hidden sm:block sm:ml-6">
                                    <div className="flex space-x-4">
                                        <NavigationDesktop />
                                    </div>
                                </div>
                            </div>
                            <div className="absolute inset-y-0 right-0 flex items-center pr-2 sm:static sm:inset-auto sm:ml-6 sm:pr-0">
                                <ConnectionStatus />
                            </div>
                        </div>
                    </div>

                    <Disclosure.Panel className="sm:hidden">
                        <div className="px-2 pt-2 pb-3 space-y-1">
                            <NavigationMobile />
                        </div>
                    </Disclosure.Panel>
                </>
            )}
        </Disclosure>
    );
};
