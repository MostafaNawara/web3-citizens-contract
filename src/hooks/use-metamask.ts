import { useEffect, useState } from 'react';

export const useMetamask = () => {
    const [isMetamaskInstalled, setIsMetamaskInstalled] = useState(false);
    const [currentNetwork, setCurrentNetwork] = useState<string | null>(null);
    const [connectedAccount, setConnectedAccount] = useState<string | null>(null);

    useEffect(() => {
        setIsMetamaskInstalled(window?.ethereum?.isMetaMask ?? false);
        setCurrentNetwork(window?.ethereum?.networkVersion ?? null);
    }, []);

    useEffect(() => {
        const fetchAccount = async () => {
            const accounts = await window?.ethereum?.request({ method: 'eth_accounts' });
            setConnectedAccount(accounts?.[0] ?? null);
        };
        fetchAccount();
    }, []);

    const enableMetaMask = async () => {
        window?.ethereum?.enable();
    };

    return {
        isMetamaskInstalled,
        isMetamaskConnected: connectedAccount !== null,
        enableMetaMask,
        currentNetwork,
        connectedAccount,
    };
};
