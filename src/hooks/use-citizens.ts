import { useCallback, useEffect, useMemo, useState } from 'react';
import Web3 from 'web3';
import citizenABI from '../smart-contracts/build/contracts/Citizen.json';

export type Citizen = {
    id: string;
    name: string;
    city: string;
    age: number;
};

const initialize = () => {
    if (typeof window === 'undefined' || window?.ethereum === 'undefined') return;
    const CONTRACT_ID = process.env.NEXT_PUBLIC_CONTRACT_ADDRESS as string;

    const web3 = new Web3(window.ethereum);
    web3.eth.defaultAccount = window?.ethereum?.eth_accounts;

    return new web3.eth.Contract(citizenABI as any, CONTRACT_ID);
};

const useCitizens = () => {
    const [citizens, setCitizens] = useState<Citizen[]>([]);
    const [isLoading, setIsLoading] = useState(true);
    const [error, setError] = useState<string | null>(null);

    const CONTRACT = useMemo(() => initialize(), []);

    const fetchCitizens = useCallback(async () => {
        setIsLoading(true);
        setError(null);

        try {
            if (!CONTRACT) return;
            const citizens = await CONTRACT.getPastEvents('Citizen', {
                fromBlock: 0,
                toBlock: 'latest',
            });

            const data = citizens?.map(citizen => {
                const { id, name, city, age } = citizen.returnValues;
                return { id, name, city, age };
            });

            setCitizens(data);
        } catch (error) {
            const err = error as Error;
            setError(err?.message);
        } finally {
            setIsLoading(false);
        }
    }, [CONTRACT]);

    useEffect(() => {
        fetchCitizens();
    }, [fetchCitizens]);

    return { citizens, isLoading, error };
};

export const useCitizensPagination = () => {
    const { citizens, isLoading, error } = useCitizens();
    const [currentPage, setCurrentPage] = useState(1);
    const [citizensPerPage, setCitizensPerPage] = useState(5);
    const totalPages = Math.ceil(citizens.length / citizensPerPage);

    const setPage = (pageNumber: number) => setCurrentPage(pageNumber);
    const setNextPage = () => setCurrentPage(currentPage + 1);
    const setPreviousPage = () => setCurrentPage(currentPage - 1);
    const isNextEnabled = currentPage < totalPages;
    const isPreviousEnabled = currentPage > 1;

    const setPageSize = useCallback(
        (pageSize: number) => {
            setCurrentPage(1);
            setCitizensPerPage(pageSize);
        },
        [setCurrentPage, setCitizensPerPage],
    );

    const indexOfLastCitizen = currentPage * citizensPerPage;
    const indexOfFirstCitizen = indexOfLastCitizen - citizensPerPage;
    const currentCitizens = citizens.slice(indexOfFirstCitizen, indexOfLastCitizen);

    return {
        citizens: currentCitizens,
        isLoading,
        error,
        currentPage,
        total: citizens.length,
        citizensPerPage,
        setPage,
        setNextPage,
        setPreviousPage,
        setPageSize,
        totalPages,
        isNextEnabled,
        isPreviousEnabled,
    };
};

export const useCitizenNote = (citizenId: string) => {
    const [note, setNote] = useState<string>('');
    const [isLoading, setIsLoading] = useState(true);
    const [error, setError] = useState<string | null>(null);

    const CONTRACT = useMemo(() => initialize(), []);

    const fetchNote = useCallback(async () => {
        setIsLoading(true);
        setError(null);

        try {
            if (!CONTRACT) return;
            const note = await CONTRACT.methods.getNoteByCitizenId(citizenId).call();

            setNote(note);
        } catch (error) {
            const err = error as Error;
            setError(err?.message);
        } finally {
            setIsLoading(false);
        }
    }, [CONTRACT, citizenId]);

    useEffect(() => {
        fetchNote();
    }, [fetchNote]);

    return { note, isLoading, error };
};

export const useAddCitizenMutation = () => {
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState<string | null>(null);

    const CONTRACT = useMemo(() => initialize(), []);

    const addCitizen = useCallback(
        async (citizen: Omit<Citizen, 'id'> & { someNote: string }) => {
            const { name, city, age, someNote } = citizen;

            setIsLoading(true);
            setError(null);

            try {
                if (!CONTRACT) return;
                await CONTRACT.methods.addCitizen(age, city, name, someNote).send({
                    from: window.ethereum.selectedAddress,
                });
            } catch (error) {
                const err = error as Error;
                setError(err?.message);
            } finally {
                setIsLoading(false);
            }
        },
        [CONTRACT],
    );

    return { addCitizen, isLoading, error };
};
