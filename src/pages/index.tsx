import type { NextPage } from 'next';
import Head from 'next/head';
import { CheckCircleIcon, InformationCircleIcon, XCircleIcon } from '@heroicons/react/outline';

import { useMetamask } from '@hooks/use-metamask';

const HomePage: NextPage = () => {
    const {
        isMetamaskInstalled,
        isMetamaskConnected,
        currentNetwork,
        connectedAccount,
        enableMetaMask,
    } = useMetamask();

    const ethereumNetwork = (networkId: number) => {
        switch (networkId) {
            case 1:
                return 'Mainnet';
            case 3:
                return 'Ropsten(Testnet)';
            case 4:
                return 'Rinkeby(Testnet)';
            case 42:
                return 'Kovan(Testnet)';
            default:
                return 'Unknown(Testnet)';
        }
    };
    return (
        <>
            <Head>
                <title>HomePage</title>
            </Head>

            <div className="md:max-w-[50%] m-auto">
                <div className="py-2">
                    <div
                        className={`flex items-center w-full bg-white leading-none text-${
                            isMetamaskInstalled ? 'green' : 'red'
                        }-600 rounded p-2 shadow text-teal text-sm`}
                    >
                        <span
                            className={`inline-flex bg-${
                                isMetamaskInstalled ? 'green' : 'red'
                            }-600 text-white rounded-full h-6 px-2 justify-center items-center`}
                        >
                            {isMetamaskInstalled ? (
                                <CheckCircleIcon className="w-4 h-4" />
                            ) : (
                                <XCircleIcon className="w-4 h-4" />
                            )}
                        </span>
                        <span className="inline-flex px-2">
                            {isMetamaskInstalled
                                ? 'MetaMask installed'
                                : 'MetaMask is not installed, please install MetaMask'}
                        </span>
                    </div>
                </div>

                <div className="py-2">
                    <div
                        className={`flex items-center bg-white leading-none text-${
                            isMetamaskConnected ? 'green' : 'red'
                        }-600 rounded p-2 shadow text-teal text-sm`}
                    >
                        <span
                            className={`inline-flex bg-${
                                isMetamaskConnected ? 'green' : 'red'
                            }-600 text-white rounded-full h-6 px-2 justify-center items-center`}
                        >
                            {isMetamaskConnected ? (
                                <CheckCircleIcon className="w-4 h-4" />
                            ) : (
                                <XCircleIcon className="w-4 h-4" />
                            )}
                        </span>
                        <span className="inline-flex px-2">
                            {isMetamaskConnected
                                ? 'MetaMask connected'
                                : 'MetaMask is not connected, please connect'}
                        </span>
                    </div>
                </div>
                {connectedAccount ? (
                    <div className="py-2">
                        <div className="flex items-center bg-white leading-none text-indigo-600 rounded p-2 shadow text-teal text-sm">
                            <span className="inline-flex bg-indigo-600 text-white rounded-full h-6 px-2 justify-center items-center">
                                <InformationCircleIcon className="w-4 h-4" />
                            </span>
                            <span className="inline-flex px-2">
                                <strong className="pr-1">Connected Account:</strong>{' '}
                                {connectedAccount}
                            </span>
                        </div>
                    </div>
                ) : null}

                {currentNetwork ? (
                    <div className="py-2">
                        <div className="flex items-center bg-white leading-none text-indigo-600 rounded p-2 shadow text-teal text-sm">
                            <span className="inline-flex bg-indigo-600 text-white rounded-full h-6 px-2 justify-center items-center">
                                <InformationCircleIcon className="w-4 h-4" />
                            </span>
                            <span className="inline-flex px-2">
                                <strong className="pr-1">Current Network:</strong>{' '}
                                {ethereumNetwork(parseInt(currentNetwork, 10))}
                            </span>
                        </div>
                    </div>
                ) : null}

                {isMetamaskInstalled && !isMetamaskConnected && (
                    <div className="mt-2">
                        <button
                            type="button"
                            onClick={() => enableMetaMask()}
                            className="focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-700 transition duration-150 ease-in-out hover:bg-green-600 bg-green-700 rounded text-white px-8 py-2 text-sm"
                        >
                            Click to enable MetaMask
                        </button>
                    </div>
                )}
            </div>
        </>
    );
};

export default HomePage;
