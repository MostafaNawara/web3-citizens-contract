import Head from 'next/head';
import type { NextPage } from 'next/types';
import { useMetamask } from '@hooks/use-metamask';
import { AddNewCitizen } from '@components/add-new-citizen';

const AddNewCitizenPage: NextPage = () => {
    const { isMetamaskInstalled } = useMetamask();

    return (
        <>
            <Head>
                <title>Add New Citizen</title>
            </Head>

            <div>
                {isMetamaskInstalled ? (
                    <AddNewCitizen />
                ) : (
                    <div className="text-center">
                        <h1>Please check MetaMask connection!</h1>
                    </div>
                )}
            </div>
        </>
    );
};

export default AddNewCitizenPage;
