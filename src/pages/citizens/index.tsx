import Head from 'next/head';
import type { NextPage } from 'next/types';
import { CitizensListing } from '@components/citizens-listing';
import { useMetamask } from '@hooks/use-metamask';

const CitizensPage: NextPage = () => {
    const { isMetamaskInstalled } = useMetamask();

    return (
        <>
            <Head>
                <title>Citizens</title>
            </Head>
            {isMetamaskInstalled ? (
                <CitizensListing />
            ) : (
                <div className="text-center">
                    <h1>Please check MetaMask connection!</h1>
                </div>
            )}
        </>
    );
};

export default CitizensPage;
