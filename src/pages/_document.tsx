import Document, { Html, Head, Main, NextScript } from 'next/document';

export default class MyDocument extends Document {
    render() {
        return (
            <Html lang="en">
                <Head>
                    <meta charSet="utf-8" content="text/html" />
                    <meta name="description" content="Citizens | Smart Contract App" />
                    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
                    <link rel="preconnect" href="https://fonts.googleapis.com" />
                    <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin="crossorigin" />
                    <link
                        href="https://fonts.googleapis.com/css2?family=Roboto+Condensed:wght@400;700&display=swap"
                        rel="stylesheet"
                    />
                </Head>
                <body className="bg-gray-100">
                    <Main />
                    <NextScript />
                </body>
            </Html>
        );
    }
}
