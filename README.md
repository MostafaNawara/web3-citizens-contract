# web3-citizens-contract

React App for the [web3-citizens-contract](https://web3-citizens-contract.vercel.app/) project. connected with [ropsten testnet](https://ropsten.etherscan.io/address/0xb5842e2384f5b6f1dbec5e130c75e82d3803c3d3) smart contract.

In order to use the app you need to use MetaMask. If you don't have MetaMask installed, you can install it by following the instructions on [MetaMask's website](https://metamask.io/). then connect to the app with MetaMask.

Application home page preview your connection with ethereum network. You can see the account it's recommended to use Ropsten testnet.

### Pages

-   Home Page ('/) # preview your connection with ethereum network
-   Citizen Page ('/citizens') # view citizen list
-   Add New Citizen Page ('/citizens/add-new') # add new citizen

### Folder structure

```
    public // => static images, icons, etc
    src
        |-- components // => Shared components (Header, Layout, etc)
        |   |-- add-new-citizen // => Add new citizen page components
        |   |-- citizens-listing // => Citizens listing components
        |-- hooks  // => react hooks to handel data fetching &  web3 connection
        |   |--
        |-- pages // => App pages
        |   |    --
        |   |-- _app.tsx // => App Layout, global styles
        |   |-- _document.tsx // => App Head, meta data & etc
        |   |-- index.tsx // => App entry point
        `-- types // => Types
```

### Used libraries and tools

-   React
-   Next.js
-   TypeScript
-   [Web3](https://web3js.readthedocs.io/en/v1.2.1/web3-eth-contract.html)
-   formik (Form state management library)
-   Tailwind CSS (Styling & Theming)
-   create-next-app
-   prettier (Code Formatting)
-   lint-staged
-   husky (Git Hooks)
-   Eslint (Code Linting)
-   Conventional Commits (Git commit messages linting)
-   Vercel (Deploying)
-   Jest & React Testing Library for unit and UI testing (ToD0)
-   Cypress for E2E Testing (ToDo)

### Getting Started

First, run the development server:

```bash
npm install
# or
yarn
```

Then, run the development server:

```bash
npm run dev
# or
yarn dev
```

Then, open the browser and navigate to the following URL: [http://localhost:3000](http://localhost:3000).

## Environment variables

Duplicate `.env.example` file and rename it to `.env.local`
then add the following variables:

```
NEXT_PUBLIC_CONTRACT_ADDRESS=your_contract_address
```

### Screenshots

#### Connection with Metamask

<img src="./public/screens/1.png" alt="">

#### Home Page

<img src="./public/screens/2.png" alt="">

#### Citizens Listing

<img src="./public/screens/3.png" alt="">

#### Add New Citizen

<img src="./public/screens/4.png" alt="">

## Demo

[https://web3-citizens-contract.vercel.app/](https://web3-citizens-contract.vercel.app/)
